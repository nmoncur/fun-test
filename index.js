const express = require('express')
const app = express()
const ContactController = require('./app/contacts/ContactController')

app.use(express.json())

app.get('/filters', ContactController.index)

module.exports = app.listen(3030, () => console.log('Running!'))
