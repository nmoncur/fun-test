# Fun Tests Project
#### This project is for random javascript exercises and tests

Once you have node installed, you should be able to start the app with:

```
node index.js
```

To run tests, run:

```
npm test
```

and it will run all unit tests.

*You will run into an uncaught error if node is running when you run the test suite*

## Filters Test
### This is a test of your collection filtering skills

There is a json object that contains a list of contacts.

The app keeps track of the last time the contact was contacted, either by email, call, or text.

We want to get contacts that have been contacted by any combination of the three different contact methods. (e.g, just called, called and texted, not contacted at all, etc).

Your test is to make the unit tests pass. You _CANNOT_ install any additional libraries that are not already included in the package.json