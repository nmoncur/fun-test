const faker = require('faker')

module.exports = {

  fake (type) {
    const types = {
      contact: {
        name: faker.name.findName(),
        email: faker.internet.email(),
        phoneNumber: faker.phone.phoneNumber(),
        activitySummary: {
          lastCall: faker.date.past(),
          lastEmail: faker.date.past(),
          lastText: faker.date.past()
        }
      }
    }

    return types[type]
  },

  create (type, count) {
    const data = []

    for (let i = 0; i < count; i++) {
      data.push(this.fake(type))
    }

    return data
  }

}
