const app = require('../..')
const expect = require('chai').expect
const request = require('supertest')

describe('ContactControllerTest', () => {
  it('should return all contacts', (done) => {
    request(app)
      .get('/filters')
      .expect(200)
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(50)
        done()
      })
  })

  it('should return contacts with no contact', (done) => {
    request(app)
      .get('/filters?called=false&emailed=false&texted=false')
      .expect(200)
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(5)
        done()
      })
  })

  it('should return only called contacts', (done) => {
    request(app)
      .get('/filters?called=true')
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(8)
        done()
      })
  })

  it('should return only emailed contacts', (done) => {
    request(app)
      .get('/filters?emailed=true')
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(12)
        done()
      })
  })

  it('should return only texted contacts', (done) => {
    request(app)
      .get('/filters?texted=true')
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(12)
        done()
      })
  })

  it('should return called & texted contacts', (done) => {
    request(app)
      .get('/filters?called=true&texted=true')
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(12)
        done()
      })
  })

  it('should return called & emailed contacts', (done) => {
    request(app)
      .get('/filters?called=true&emailed=true')
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(3)
        done()
      })
  })

  it('should return emailed & texted contacts', (done) => {
    request(app)
      .get('/filters?emailed=true&texted=true')
      .end((err, res) => {
        if (err) throw new Error(err)
        expect(res.body).to.be.an('array')
        expect(res.body).to.have.lengthOf(9)
        done()
      })
  })
})
