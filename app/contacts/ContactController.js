const filter = require('./ContactFilters')

module.exports = {

  index (req, res) {
    let data = require('./ContactData.json')

    data = filter.exec(data, req.query)

    res.status(200).json(data)
  }

}
